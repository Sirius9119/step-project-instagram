import React, {useEffect, useState} from 'react';
import {ReactComponent as Logo} from "../../components/icon/InstaIcon/Instagram-Logo.svg";
import {Link, Outlet, useOutletContext} from "react-router-dom";

function PageWrapper() {

    localStorage.setItem('hashUser', JSON.stringify("10j3e4567-e89b-12d3-a456-426655440010"))
    const [isModalPost, setIsModalPost] = useState(false)
    const [loginUser, setLoginUser] = useState({})
    const [usersData, setUsersData] = useState([])
    const [followData, setFollowData] = useState([])
    const [likesData, setLikesData] = useState([])
    const [commentsData, setCommentsData] = useState([])
    const [postData, setPostData] = useState([])
    const [currentPost, setCurrentPost] = useState({})
    // const [isLiked, setIsLiked] = useState(false)
    // const INSTANCE_URL = "http://localhost:8080/api/posts/"
    const INSTANCE_URL = "https://step-project-instagram-back-end.vercel.app"

    const storedHash = JSON.parse(localStorage.getItem('hashUser'));
    useEffect(() => {
        fetch(`${INSTANCE_URL}/api/users`)
            .then(res => res.json())
            .then(({users}) => {
                setUsersData(users)
            })

    }, [])
    useEffect(() => {

        fetch(`${INSTANCE_URL}/api/likes`)
            .then(res => res.json())
            .then(({likes}) => {
                setLikesData(likes)
            })
    }, [])

    useEffect(() => {
        fetch(`${INSTANCE_URL}/api/comments`)
            .then(res => res.json())
            .then(({comments}) => {
                setCommentsData(comments)
            })
    }, [])
    useEffect(() => {
        fetch(`${INSTANCE_URL}/api/posts`)
            .then(res => res.json())
            .then(({posts}) => {
                setPostData(posts)
            })
    }, [])
    useEffect(() => {
        function setLoginUserFunction(hash) {
            const user = usersData.find(user => user.hash === hash);
            if (user) {
                setLoginUser(user);
            }
        }

        setLoginUserFunction(storedHash);
    }, [usersData, storedHash]);

    // useEffect(() => {
    //     fetch('http://localhost:8080/api/follows', {
    //         method: 'POST',
    //         headers: {
    //             'Content-Type': 'application/json'
    //         },
    //         body: JSON.stringify({
    //             user_id: loginUser.ID
    //         })
    //     })
    //         .then(res => res.json())
    //         .then(({follows}) => {
    //             setUsersFollow(follows)
    //         })
    // }, [loginUser.ID])

    useEffect(() => {
        fetch(`${INSTANCE_URL}/api/follows`)
            .then(res => res.json())
            .then(({follows}) => {
                setFollowData(follows)
            })
    }, [loginUser.ID])



    const handleCurrentPost = (current) => {
        setCurrentPost({...current})
    }
    const handleModal = () => {
        setIsModalPost(!isModalPost);
    }
    const handleOutside = (event) => {
        if (!event.target.closest(".modal")) {
            handleModal()
        }
    }
    function toggleFollow(userToFollow, isFollow) {
        if (!isFollow) {
            addFollow(userToFollow)
        } else {
            deleteFollow(userToFollow)
        }
    }
    function addFollow(userToFollow) {
        fetch(`${INSTANCE_URL}/api/follow`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                userToFollow_id: userToFollow,
                user_id: loginUser.ID
            })
        })
            .then(res => res.json())
            .then(({status}) => {
                if (status) {
                    setFollowData([...followData, {userToFollow_id: userToFollow, user_id: loginUser.ID}])
                }
            })
    }
    function deleteFollow(userToFollow) {
        fetch(`${INSTANCE_URL}/api/follow/del`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                userToFollow_id: userToFollow,
                user_id: loginUser.ID
            })
        }).then(res => res.json())
            .then(({status}) => {
                if (status) {
                    const newFollows = followData.filter(follow => !(follow.user_id === loginUser.ID && follow.userToFollow_id === userToFollow));
                    setFollowData(newFollows)
                }
            })
    }



    function toggleLike(postId, isLiked) {
        if (!isLiked) {
            addLike(postId)
        } else {
            deleteLike(postId)
        }
    }

    function deleteLike(postId) {
        fetch(`${INSTANCE_URL}/api/like/del`, {
            method: 'DELETE',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                post_id: postId,
                user_id: loginUser.ID
            })
        }).then(res => res.json())
            .then(({status}) => {
                if (status) {
                    const newLikes = likesData.filter(like => !(like.user_id === loginUser.ID && like.post_id === postId));

                    setLikesData(newLikes)
                }
            })
    }

    function addLike(postId) {
        fetch(`${INSTANCE_URL}/api/like`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                post_id: postId,
                user_id: loginUser.ID
            })
        })
            .then(res => res.json())
            .then(({status}) => {
                if (status) {
                    setLikesData([...likesData, {post_id: postId, user_id: loginUser.ID}])
                }
            })

    }


    return (
        <>  <Link to='/'>
            <Logo/>
        </Link>

            <main className="container flex-wrap">
                <Outlet
                    context={{
                        isModalPost,
                        handleModal,
                        handleOutside,
                        usersData,
                        loginUser,
                        followData,
                        likesData,
                        commentsData,
                        postData,
                        handleCurrentPost,
                        currentPost,
                        toggleLike,
                        toggleFollow
                        // isLiked,
                        // handleLike

                    }}
                />
            </main>

        </>
    )
}

export default PageWrapper