import './Modal.scss';
import PropTypes from "prop-types";
import MoreIcon from "../icon/MoreIcon/MoreIcon.jsx";
import React, {useEffect, useState} from "react";
import CommentModal from "../CommentModal/index.js";
import HeartIcon from "../icon/HeartIcon/HeartIcon.jsx";
import CommentIcon from "../icon/CommentIcon/CommentIcon.jsx";
import SendIcon from "../icon/SendIcon/SendIcon.jsx";
import SaveIcon from "../icon/SaveIcon/SaveIcon.jsx";
import AddComment from "../AddComment/AddComment.jsx";
import {useOutletContext} from "react-router-dom";

export default function Modal({handleOutside}) {
    const {
        currentPost,
        usersData,
        likesData,
        commentsData,
        postData,
        toggleLike,
        loginUser


    } = useOutletContext()
    const [isLiked, setIsLiked] = useState(false)
    const handleLike = () => {
        setIsLiked(!isLiked)
    }


    useEffect(()=> {
        if(likesData.some(like => like.post_id === post.post_id && like.user_id === loginUser.ID)) {
            setIsLiked(true);
        }
    },[likesData, loginUser.ID]);

    const allCommentsPost = commentsData.filter(comment => comment.post_id === currentPost.postId)
    allCommentsPost.sort((a, b) => b.date - a.date)
    const post = postData.find(post => post.post_id === currentPost.postId)
    const user = usersData.find(user => user.ID === post.user_id)
    const allLikesPost = likesData.filter(like => like.post_id === currentPost.postId)
    const usernamesLikeArray = allLikesPost.map(post => {
        const user = usersData.find(userData => userData.ID === post.user_id);

        return user
            ? {username: user.username, avatar: user.avatar}
            : {username: null, avatar: null};
    });


    return (
        <div onClick={(event) => handleOutside(event)} className="modal-wrapper">
            <div className="modal">
                <div className="modal-box">
                    <div className="modal__img-container" onDoubleClick={()=>{
                        handleLike()
                        toggleLike(post.post_id, isLiked)
                    } }>
                        <img className="modal__img" src={post.post_image} alt="image"/>
                    </div>
                    <div className="modal__content-container">
                        <div className="modal__user-container">
                            <div className='modal__author-avatar'>
                                <img src={user.avatar} alt='avatar'/>
                            </div>
                            <p className='modal__author-name'>{user.username}</p>
                            <MoreIcon/>
                        </div>
                        <div className='modal__comments-wrapper'>
                            {allCommentsPost.map(comment => {
                                const commentAuthor = usersData.find(user => user.ID === comment.user_id);

                                return (
                                    <CommentModal
                                        comment_author={commentAuthor.username}
                                        comment_text={comment.comment_text}
                                        commentAuthorAvatar={commentAuthor.avatar}
                                        key={comment._id}

                                    />
                                )

                            })}
                            {allCommentsPost.length === 0 &&
                                <p className='modal__no-comments'>Тут еще нет коментариев</p>}


                        </div>
                        <div className='main-post__icon-wrapper'>
                            <HeartIcon
                                isLiked={isLiked}
                                handleLike={() => {
                                    handleLike()
                                    toggleLike(post.post_id, isLiked)
                                }}
                            />
                            <CommentIcon/>
                            <SendIcon/>
                            <SaveIcon/>
                        </div>
                        {usernamesLikeArray.length !== 0 &&

                            <div className='modal__liked-wrapper'>
                                <div className='modal__liked-avatar'>
                                    <img src={usernamesLikeArray[usernamesLikeArray.length - 1].avatar} alt='avatar'/>
                                </div>
                                <p>Нарвится</p>
                                <p>{usernamesLikeArray[usernamesLikeArray.length - 1].username}
                                    {usernamesLikeArray.length > 1
                                        &&
                                        <span> и еще {usernamesLikeArray.length - 1}</span>}
                                </p>


                            </div>

                        }


                        <AddComment/>
                    </div>


                </div>
            </div>
        </div>
    )

}

Modal.propTypes = {

    handleOutside: PropTypes.func,
    currentPost: PropTypes.object

}