import React, {useState} from 'react';
import './AddComment.scss'

function AddComment(props) {
    // const hideButton = {
    //     display: 'none'
    // };
    const [comment, setComment] = useState('')
    const handleInputChange = (event) => {
        setComment(event.target.value);
    };


    return (
        <form className="comment-section">
            <input
                type="text"
                placeholder="Добавить комментарий..."
                value={comment}
                onChange={handleInputChange}
            />
            {comment.trim() !== '' && (
                <button className="submit-btnComment">Отправить</button>
            )}
        </form>
    );
}

export default AddComment;