import express from 'express';
import mongoose from'mongoose';
import bodyParser from 'body-parser';
import cors from 'cors';
import userMongo from "./routers/user.js";
import postMongo from "./routers/post.js";
import commentMongo from "./routers/comment.js";
import likeMongo from "./routers/like.js"
import followMongo from "./routers/follow.js"

const MONGO_URL = "mongodb+srv://mongodata:admin12345@cluster0.6zydyrm.mongodb.net/instagram?retryWrites=true&w=majority"
const urlEncodedParser = bodyParser.urlencoded({extended:false})
const app = express();



app.use(express.urlencoded({ extended: false }));
app.use(express.json());
app.use(cors());

app.use('/api', urlEncodedParser, userMongo)
app.use('/api', urlEncodedParser, postMongo)
app.use('/api', urlEncodedParser, commentMongo)
app.use('/api', urlEncodedParser, likeMongo)
app.use('/api', urlEncodedParser, followMongo)

app.all('*', (req, res) => {
    res.status(404).send('<h1>resource not found</h1>');
})
const port = process.env.PORT || 8080;

try {
    mongoose.connect(MONGO_URL, {
        useNewUrlParser: true
    })
    app.listen(port, () =>{
        console.info(`Server listening on port ${port}`)
    });

} catch (error) {
    console.log(error)
}
