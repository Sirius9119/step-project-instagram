import {Schema, model} from 'mongoose';


const userMongo = new Schema({

    ID: String,
    username: String,
    email: String,
    hash: String,
    password: String,
    registration_date:{ type: Number, default: Date.now },
    real_name: String,
    avatar: String,
    follows: Array,
    followings: Array
})
export default model('User', userMongo);
