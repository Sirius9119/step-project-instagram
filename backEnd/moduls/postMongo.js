import {Schema, model} from 'mongoose';


const postMongo = new Schema({
    post_id: String,
    user_id: String,
    post_text: String,
    post_image: String,
    post_date: { type: Number, default: Date.now },
})
    export default model('Post', postMongo);