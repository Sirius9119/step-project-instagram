import {Router} from 'express';
import likeMongo from "../moduls/likeMongo.js";
import {v4 as uuidv4} from 'uuid';


const router = Router();

router.get('/likes', async (req, res) => {
    try {
        const likes = await likeMongo.find();

        res.status(200).json({status: true, likes});
    } catch (err) {
        res.status(200).json({status: false, error: err.message})
    }
})

router.post('/like', async (req, res) => {
    const {user_id, post_id} = req.body


    const newLike = {
        user_id: user_id,
        post_id: post_id,
        like_id: uuidv4(),

    }
    const like = new likeMongo(newLike)

    try {
        await like.save()

        res.status(200).json({status: true})
    } catch (err) {
        res.status(200).json({status: false, error: err.message})
    }

})

router.delete('/like/del', async (req, res) => {
    const {post_id, user_id} = req.body

    try {
        await likeMongo.deleteOne({post_id: post_id, user_id: user_id})

        res.status(200).json({status: true})
    } catch (err) {
        res.status(200).json({status: false, error: err.message})
    }
})

export default  router